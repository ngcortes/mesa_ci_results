import bjoern, multiprocessing, os
from results import app

worker_pids = []

bjoern.listen(wsgi_app=app,
              host='0.0.0.0',
              port=8000,
              reuse_port=True)

for _ in range(int(multiprocessing.cpu_count()/2)):
   pid = os.fork()
   if pid > 0:
     worker_pids.append(pid)
   elif pid == 0:
     try:
        bjoern.run()
     except KeyboardInterrupt:
        pass
     exit()

try:
   for _ in worker_pids:
      os.wait()
except KeyboardInterrupt:
   for pid in worker_pids:
      os.kill(pid, signal.SIGINT)
