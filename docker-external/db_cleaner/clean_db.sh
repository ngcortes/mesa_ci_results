#!/bin/bash

# MySQL server info
# Get database sizes sorted by size (largest first)
database_sizes=$(mysql -e "SELECT table_schema, SUM(data_length + index_length) as size FROM information_schema.tables GROUP BY table_schema ORDER BY size ASC;" | grep -Ev "(table_schema|information_schema|mysql|performance_schema|sys)")
echo $database_sizes
while IFS= read -r line; do
    db=$(echo "$line" | awk '{print $1}')

    # Loop through tables
    tables=$(mysql -D $db -e "SHOW TABLES;" | grep -v "Tables_in")
    echo $tables
    for table in $tables; do
	query="SELECT data_length + index_length FROM information_schema.tables WHERE table_schema = '$db' AND table_name = '$table';"

	table_size=$(mysql -N -s -e "$query")
	echo "Table size for $db.$table before cleanup: $table_size"

	mysql $db -e "alter table $table engine=InnoDB;"

	table_size=$(mysql -N -s -e "$query")
	echo "Table size for $db.$table after cleanup: $table_size"
    done
done <<< "$database_sizes"
